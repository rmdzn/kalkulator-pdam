function hargaAirGol() {
    var select = document.getElementById('gol');
    var val = select.options[select.selectedIndex].value;
    var gol = JSON.parse(val).gol;
    // console.log(value);

    switch(gol) {
    case "sosum":
	hargaAwal = 2750;
	hargaTengah = 2750;
	hargaAkhir = 3000;
	break;
    case "soskhu":
	hargaAwal = 2750;
	hargaTengah = 2750;
	hargaAkhir = 3000;
	break;
    case "rta1":
	hargaAwal = 3650;
	hargaTengah = 5000;
	hargaAkhir = 5250;
	break;
    case "rta2":
	hargaAwal = 4100;
	hargaTengah = 5900;
	hargaAkhir = 6300;
	break;
    case "rta3":
	hargaAwal = 5750;
	hargaTengah = 6250;
	hargaAkhir = 6850;
	break;
    case "rta4":
	hargaAwal = 6050;
	hargaTengah = 6400;
	hargaAkhir = 6900;
	break;
    case "rtb":
	hargaAwal = 6200;
	hargaTengah = 6700;
	hargaAkhir = 7300;
	break;
    case "inspem":
	hargaAwal = 6050;
	hargaTengah = 7100;
	hargaAkhir = 9250;
	break;
    case "niagakecil":
	hargaAwal = 7400;
	hargaTengah = 7400;
	hargaAkhir = 9250;
	break;
    case "industrikecil":
	hargaAwal = 8350;
	hargaTengah = 8350;
	hargaAkhir = 11500;
	break;
    case "niagabesar":
	hargaAwal = 10350;
	hargaTengah = 10350;
	hargaAkhir = 13000;
	break;
    case "industribesar":
	hargaAwal = 11600;
	hargaTengah = 11600;
	hargaAkhir = 13500;
	break;
    default:
	hargaAwal = 0;
	hargaTengah = 0;
	hargaAkhir = 0;
    }
    return [hargaAwal, hargaTengah, hargaAkhir];
}

function calHarga(angkameter, kelas) {
    if (angkameter <= 10) {
        if (kelas == "1") {
            pakai = 10*hargaAirGol()[0];
        }
    } else if (angkameter <= 20) {
	pakai = (10*hargaAirGol()[0])+((angkameter-10)*hargaAirGol()[1]);
    } else if (angkameter > 20) {
	pakai = (10*hargaAirGol()[0])+(10*hargaAirGol()[1])+((angkameter-20)*hargaAirGol()[2]);
    } else {
	alert("Harus angka minimal 0");
	return false;
    }
    return pakai;
}

function calReset() {
    var angkameter = document.getElementById('angkameter');
    
    document.getElementById('biayapakai').innerHTML = "0";
    document.getElementById('total').innerHTML = "<mark>0</mark>";
    angkameter.focus();
    angkameter.value = "";
}

function changedBiayaPelihara() {
    var select = document.getElementById('wm');
    var ukuran = select.options[select.selectedIndex].value;

    switch(ukuran) {
    case "tigaperempat":
	biaya = "17500";
	break;
    case "satu":
	biaya = "20000";
	break;
    case "satuseperempat":
	biaya = "30000";
	break;
    case "satusetengah":
	biaya = "40000";
	break;
    case "dua":
	biaya = "60000";
	break;
    case "dualebih":
	biaya = "100000";
	break;
    default:
	biaya = "12500";
    }

    document.getElementById('biayapelihara').innerHTML = "<span id='biayapelihara'>" + biaya +"</span>";
    // console.log(biaya);
    return parseInt(biaya);
}

function changeUkuranWM() {
    var gol = document.getElementById('gol');
    if (gol.value == "niagabesar" || gol.value == "industribesar") {
	document.getElementById('wm').value = "tigaperempat";
    }
}

function calTotal(angka) {
    const adm = 7500;
    var angka = parseInt(document.getElementById('angkameter').value);
    var select = document.getElementById('gol');
    var val = select.options[select.selectedIndex].value;
    var kelas = JSON.parse(val).kelas;
    console.log(kelas);

    harga = calHarga(angka,kelas)+adm+changedBiayaPelihara();
    //console.log(calHarga(angka)+adm+changedBiayaPelihara());
    document.getElementById('biayapakai').innerHTML = pakai;
    document.getElementById('total').innerHTML = '<mark>' + harga + '</mark>';
}
