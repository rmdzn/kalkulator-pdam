function hargaAirGol() {
    var select = document.getElementById('gol');
    var value = select.options[select.selectedIndex].value;

    switch(value) {
    case "sosum":
	hargaAwal = 2750;
	hargaTengah = 2750;
	hargaAkhir = 3000;
	break;
    case "soskhu":
	hargaAwal = 2750;
	hargaTengah = 2750;
	hargaAkhir = 3000;
	break;
    case "rta1":
	hargaAwal = 3450;
	hargaTengah = 4000;
	hargaAkhir = 4750;
	break;
    case "rta2":
	hargaAwal = 3900;
	hargaTengah = 4900;
	hargaAkhir = 5800;
	break;
    case "rta3":
	hargaAwal = 4750;
	hargaTengah = 5650;
	hargaAkhir = 6350;
	break;
    case "rta4":
	hargaAwal = 4900;
	hargaTengah = 5800;
	hargaAkhir = 6400;
	break;
    case "rtb":
	hargaAwal = 5200;
	hargaTengah = 6100;
	hargaAkhir = 6700;
	break;
    case "inspem":
	hargaAwal = 4900;
	hargaTengah = 6500;
	hargaAkhir = 8750;
	break;
    case "niagakecil":
	hargaAwal = 6250;
	hargaTengah = 6800;
	hargaAkhir = 8750;
	break;
    case "industrikecil":
	hargaAwal = 7500;
	hargaTengah = 8750;
	hargaAkhir = 11000;
	break;
    case "niagabesar":
	hargaAwal = 8100;
	hargaTengah = 9750;
	hargaAkhir = 12500;
	break;
    case "industribesar":
	hargaAwal = 8500;
	hargaTengah = 11000;
	hargaAkhir = 13000;
	break;
    default:
	hargaAwal = 0;
	hargaTengah = 0;
	hargaAkhir = 0;
    }
    return [hargaAwal, hargaTengah, hargaAkhir];
}

function calHarga(angkameter) {
    if (angkameter <= 10) {
	pakai = 10*hargaAirGol()[0];
    } else if (angkameter <= 20) {
	pakai = (10*hargaAirGol()[0])+((angkameter-10)*hargaAirGol()[1]);
    } else if (angkameter > 20) {
	pakai = (10*hargaAirGol()[0])+(10*hargaAirGol()[1])+((angkameter-20)*hargaAirGol()[2]);
    } else {
	alert("Harus angka minimal 0");
	return false;
    }
    return pakai;
}

function calReset() {
    var angkameter = document.getElementById('angkameter');
    
    document.getElementById('biayapakai').innerHTML = "0";
    document.getElementById('total').innerHTML = "<mark>0</mark>";
    angkameter.focus();
    angkameter.value = "";
}

function changedBiayaPelihara() {
    var select = document.getElementById('wm');
    var ukuran = select.options[select.selectedIndex].value;

    switch(ukuran) {
    case "tigaperempat":
	biaya = "17500";
	break;
    case "satu":
	biaya = "20000";
	break;
    case "satuseperempat":
	biaya = "30000";
	break;
    case "satusetengah":
	biaya = "40000";
	break;
    case "dua":
	biaya = "60000";
	break;
    case "dualebih":
	biaya = "100000";
	break;
    default:
	biaya = "12500";
    }

    document.getElementById('biayapelihara').innerHTML = "<span id='biayapelihara'>" + biaya +"</span>";
    // console.log(biaya);
    return parseInt(biaya);
}

function changeUkuranWM() {
    var gol = document.getElementById('gol');
    if (gol.value == "niagabesar" || gol.value == "industribesar") {
	document.getElementById('wm').value = "tigaperempat";
    }
}

function calTotal(angka) {
    const adm = 7500;
    var angka = parseInt(document.getElementById('angkameter').value);

    harga = calHarga(angka)+adm+changedBiayaPelihara();
    //console.log(calHarga(angka)+adm+changedBiayaPelihara());
    document.getElementById('biayapakai').innerHTML = pakai;
    document.getElementById('total').innerHTML = '<mark>' + harga + '</mark>';
}
